package com.project.winter.Project_Name;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectNameApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectNameApplication.class, args);
	}
}
